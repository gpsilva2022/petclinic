variable "aws_key_path" {}
variable "aws_key_name" {}
#variable "rsa_key_path" {}

variable "aws_region" {
    description = "Region for the VPC"
    default = "us-east-1"
}

variable "amis" {
    description = "AMIs by region"
    default = {
        us-east-1 = "ami-467ca739" # Amazon Linux AMI
    }
}

variable "vpc_cidr" {
    description = "CIDR for the VPC"
    default = "192.168.0.0/16"
}

variable "public_subnet_cidr" {
    description = "CIDR for the Public Subnet"
    default = "192.168.1.0/24"
}

variable "private_subnet_cidr" {
    description = "CIDR for the Private Subnet"
    default = "192.168.2.0/24"
}
